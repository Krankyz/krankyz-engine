/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#pragma once

#include "KrankyzWinDefs.h"
#include "KrankyzException.h"
#include "Keyboard.h"
#include "Mouse.h"
#include <optional>

//Helper Macro for error exception
#define KRANKYZ_EXCEPT(hr) CWindowMgr::CWindowMgrException(__LINE__, __FILE__, hr)
#define KRANKYZ_LAST_EXCEPT() CWindowMgr::CWindowMgrException(__LINE__, __FILE__, GetLastError())

class CWindowMgr
{
public:
	class CWindowMgrException : public CKrankyzException
	{
	public:

		/*Constructor
		* @param line - The line code for the exception
		* @param file - The file of the exception
		* @param hr - The window error code
		*/
		CWindowMgrException(int line, const char* file, HRESULT hr);

		/*Function*/
		const char* what() const noexcept override;

		/*Function that gets the type of the exception*/
		virtual const char* GetType() const noexcept override;

		/*Function that translates the windows error code*/
		static std::string TranslateErrorCode(HRESULT hr);

		/*Function that gets the windows error code*/
		HRESULT GetErrorCode() const noexcept;

		/*Function that get the error code in a string*/
		std::string GetErrorString() const noexcept;

	private:

		/*Member variable that stores the windows error code*/
		HRESULT mHr;
	};
private:
	class CWindow
	{
	public:

		static const char* GetName() noexcept;
		static HINSTANCE GetInstance() noexcept;
	private:

		/*Constructor*/
		CWindow() noexcept;

		/*DeConstructor*/
		~CWindow();

		/*Make sure only 1 instance is possible*/
		CWindow(const CWindow&) = delete;
		CWindow& operator = (const CWindow&) = delete;

		/*Member variable to store the window name*/
		static constexpr const char* mwndClassName = "Krankyz Engine";

		/*Member variable to store the Window instance*/
		static CWindow mwndClass;

		/*Member variable to the program instance in Windows*/
		HINSTANCE mhInst;
	};
public:
	/*Constructor
	* @param width - The width of the window
	* @param height - The height of the window
	* @param name - The name of the window
	*/
	CWindowMgr(int width, int height, const char* name);

	/*DeConstructor*/
	~CWindowMgr();

	/*Make sure only 1 instance is possible*/
	CWindowMgr(const CWindowMgr&) = delete;
	CWindowMgr& operator = (const CWindowMgr&) = delete;

private:

	/*Static Callback function that setups the pointer to the the window mgr instance
	* @param hWnd - The width of the window
	* @param msg - The height of the window
	* @param wParam - The name of the window
	* @param lParam
	*/
	static LRESULT CALLBACK HandleMsgSetup(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept;

	/*Static Callback function that just adapts and calls the actual window message function
	* @param hWnd - The width of the window
	* @param msg - The height of the window
	* @param wParam - The name of the window
	* @param lParam
	*/
	static LRESULT CALLBACK HandleMsgThunk(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept;

	/*Function that handles window messages
	* @param hWnd - The width of the window
	* @param msg - The height of the window
	* @param wParam - The name of the window
	* @param lParam
	*/
	LRESULT HandleMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept;

public:

	/*Function that sets the title of the window
	* @param title - The new title of the window
	*/
	void SetWindowTitle(const std::string& title);

	/*Function that process window messages (Optional allows us to return an int as an optional value if possible)
	*/
	static std::optional<int> ProcessMessages();

public:

	/*Member variables that stores the keyboard class*/
	CKeyboard m_CKeyboard;

	/*Member variables that stores the Mouse class*/
	CMouse m_CMouse;

private:

	/*Member variables that stores the Window Width*/
	int mwidth;

	/*Member variables that stores the Window height*/
	int mheight;

	/*Member variables that stores the Window height*/
	HWND mhWnd;
};