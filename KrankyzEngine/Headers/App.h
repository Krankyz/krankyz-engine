/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#pragma once
#include "KrankyzWindow.h"
#include "KrankyzTimer.h"

class CApp
{
public:

	/*Constructor*/
	CApp();

	/*Main function that starts the app*/
	int Start();

private:

	/*Main frame update/loop function*/
	void FrameUpdate();

private:

	/*Member variable that holds the window object*/
	CWindowMgr m_Window;

	/*Member variable that holds timer object*/
	cKrankyzTimer m_timer;
};