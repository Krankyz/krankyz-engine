/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#pragma once
#include <queue>
#include <bitset>

class CKeyboard
{
	friend class CWindowMgr;

public:

	class CKeyboardEvent
	{
	public:

		/*Enum class that defines the type of keyboard events
		*/
		enum class Type
		{
			Press,
			Release,
			Invalid
		};

	public:

		/*Constructor*/
		CKeyboardEvent() noexcept : m_eType(Type::Invalid), m_szCode(0u)
		{}

		/*Constructor
		* @param type - The event type
		* @param code - The key code
		*/
		CKeyboardEvent(Type type, unsigned char code) noexcept : m_eType(type), m_szCode(code)
		{}

		/*Function that checks if the key is pressed*/
		bool IsPress() const noexcept
		{
			return m_eType == Type::Press;
		}

		/*Function that checks if the key is released*/
		bool IsRelease() const noexcept
		{
			return m_eType == Type::Release;
		}

		/*Function that checks if the key event type is valid*/
		bool IsValid() const noexcept
		{
			return m_eType != Type::Invalid;
		}

		/*Function that returns the code of the key pressed*/
		unsigned char GetCode() const noexcept
		{
			return m_szCode;
		}

	private:

		/*Member variables that stores the key event type*/
		Type m_eType;

		/*Member variables that stores the key char*/
		unsigned char m_szCode;

	};

public:

	/*Constructor set to default*/
	CKeyboard() = default;

	/*Delete copy assignment*/
	CKeyboard(const CKeyboard&) = delete;
	CKeyboard& operator=(const CKeyboard&) = delete;

	/*******************************Key Events*********************************/

	/*Function that checks if key has been pressed
	* @param keyCode - The key code of the key
	*/
	bool KeyIsPressed(unsigned char keyCode) const noexcept;

	/*Function that checks and pulls events from the event queue*/
	CKeyboardEvent ReadKey() noexcept;

	/*Function that checks if the queue is empty or not*/
	bool KeyIsEmpty() const noexcept;

	/*Function that clears the queue*/
	void FlushKey() noexcept;


	/*******************************Char Events********************************/

	/*Function that reads the character from the queue*/
	char ReadChar() noexcept;

	/*Function that checks if the characters stream is empty*/
	bool CharIsEmpty() const noexcept;

	/*Function that clears the char queue */
	void FlushChar() noexcept;

	/*Function that flushes both event and char queues*/
	void Flush() noexcept;


	/****************************AutoRepeat Control****************************/

	/*Function that enables auto repeat*/
	void EnableAutoRepeat() noexcept;

	/*Function that disables auto repeat*/
	void DisableAutoRepeat() noexcept;

	/*Function that checks if auto repeat is enables or not*/
	bool IsAutoRepeatEnabled() const noexcept;

private:
	
	/*Callback function when a key is pressed
	* @param keyCode - The key code of the key
	*/
	void OnKeyPressed(unsigned char keyCode) noexcept;

	/*Callback function when a key is released
	* @param keyCode - The key code of the key
	*/
	void OnKeyReleased(unsigned char keyCode) noexcept;

	/*Callback function when a character are detected
	* @param character - The char pressed
	*/
	void OnChar(char character) noexcept;

	/*Function that clears the bitset that holds all the key states*/
	void ClearState() noexcept;

	/*Template function that reduces the buffer when the max is exceeded
	* @param buffer - The queue to trim
	*/
	template<typename T>
	static void TrimBuffer(std::queue<T>& buffer) noexcept;

private:

	/*Member variables that stores the maximum size of virtual keys we have*/
	static constexpr unsigned int m_nKeys = 256u;

	/*Member variables that stores the maximum size of the key bitset buffer*/
	static constexpr unsigned int m_nBufferSize = 16u;

	/*Member variables that set auto repeat on or off*/
	bool m_bAutoRepeatEnabled = false;

	/*Member variables that packs the 256 key bools into single bits*/
	std::bitset<m_nKeys> m_KeyStates;

	/*Queue for the key events*/
	std::queue<CKeyboardEvent> m_KeyBuffer;

	/*Queue for the char events*/
	std::queue<char> m_CharBuffer;
};
