/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#pragma once
#include <exception>
#include <string>

class CKrankyzException : public std::exception
{
public:

	/*Constructor
	* @param line - The line code for the exception
	* @param file - The file of the exception
	*/
	CKrankyzException(int line, const char* file) noexcept;

	/*Function*/
	const char* what() const noexcept override;

	/*Function that gets the type of the exception*/
	virtual const char* GetType() const noexcept;

	/*Function that gets the line code of the exception*/
	int GetLine() const noexcept;

	/*Function that gets the file of the exception*/
	const std::string& GetFile() const noexcept;

	/*Function that gets the origin string of the exception*/
	std::string GetOriginString() const noexcept;

private:

	/*Member variables that stores the line number of the exception*/
	int mLine;

	/*Member variables that stores the file from where the exception is from*/
	std::string mstrFile;

protected:

	/*Member variables that stores the file and the origin strong
	* Saved as mutable because the function what is const and it needs to get edited
	*/
	mutable std::string mstrWhatBuffer;

};
