/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#pragma once
#include <queue>
#include <utility>

class CMouse
{
	friend class CWindowMgr;

public:

	class CMouseEvent
	{
	public:

		/*Enum class that defines the type of Mouse events
		*/
		enum class Type
		{
			LPress,
			LRelease,
			RPress,
			RRelease,
			WheelUp,
			WheelDown,
			Move,
			MiddlePressed,
			MiddleReleased,
			Enter,
			Leave,
			Invalid
		};

	public:

		/*Constructor*/
		CMouseEvent() noexcept 
			: m_eType(Type::Invalid), m_bLeftIsPressed(false), m_bRightIsPressed(false), m_bMiddleIsPressed(false), m_nX(0), m_nY(0)
		{}

		/*Constructor
		* @param type - The event type
		* @param m_bLeftIsPressed - Left mouse button pressed boolean
		* @param m_bRightIsPressed - Right mouse button pressed boolean
		* @param x - Mouse x position
		* @param y - Mouse y position
		*/
		CMouseEvent(Type type, const CMouse& parent) noexcept 
			: m_eType(type), m_bLeftIsPressed(parent.m_bLeftIsPressed), m_bRightIsPressed(parent.m_bRightIsPressed), m_bMiddleIsPressed(parent.m_bMiddleIsPressed), m_nX(parent.m_nX), m_nY(parent.m_nY)
		{}

		/*Function that checks if the mouse event type is valid */
		bool IsValid() const noexcept
		{
			return m_eType != Type::Invalid;
		}

		/*Function that returns the mouse event type */
		Type GetType() const noexcept
		{
			return m_eType;
		}

		/*Function that returns the mouse x and y positions */
		std::pair<int, int> GetPosition() const noexcept
		{
			return { m_nX,m_nY };
		}

		/*Function that returns the mouse x position */
		int GetPositionX() const noexcept
		{
			return m_nX;
		}

		/*Function that returns the mouse y position */
		int GetPositionY() const noexcept
		{
			return m_nY;
		}

		/*Function that returns true if left mouse button is pressed */
		bool IsLeftButtonPressed() const noexcept
		{
			return m_bLeftIsPressed;
		}

		/*Function that returns true if right mouse button is pressed  */
		bool IsRightButtonPressed() const noexcept
		{
			return m_bRightIsPressed;
		}

		/*Function that returns true if middle mouse button is pressed  */
		bool IsMiddletButtonPressed() const noexcept
		{
			return m_bMiddleIsPressed;
		}

	private:

		/*Member variables that stores the key event type*/
		Type m_eType;

		/*Member variables bool that says if left mouse button is pressed*/
		bool m_bLeftIsPressed;

		/*Member variables bool that says if right mouse button is pressed*/
		bool m_bRightIsPressed;

		/*Member variables bool that says if middle mouse button is pressed*/
		bool m_bMiddleIsPressed;

		/*Member variables that saves the mouse x position*/
		int m_nX;

		/*Member variables that saves the mouse y position*/
		int m_nY;

	};

public:

	/*Constructor set to default*/
	CMouse() = default;

	/*Delete copy assignment*/
	CMouse(const CMouse&) = delete;
	CMouse& operator=(const CMouse&) = delete;

	/*Function that returns the mouse x and y positions*/
	std::pair<int, int> GetPosition() const noexcept;

	/*Function that returns the mouse x position*/
	int GetPositionX() const noexcept;

	/*Function that returns the mouse y position*/
	int GetPositionY() const noexcept;

	/*Function that returns true if left mouse button is pressed*/
	bool IsLeftButtonPressed() const noexcept;

	/*Function that returns true if right mouse button is pressed*/
	bool IsRightButtonPressed() const noexcept;

	/*Function that returns true if middle mouse button is pressed*/
	bool IsMiddleButtonPressed() const noexcept;

	/*Function that returns true if the mouse is inside the app window*/
	bool IsMouseInWindow() const noexcept;

	/*Function that checks and pulls mouse events from the queue*/
	CMouse::CMouseEvent Read() noexcept;

	/*Function that checks if the queue is empty */
	bool IsEmpty() const noexcept
	{
		return m_MouseEventBuffer.empty();
	}

	/*Function that flushes both event and char queues*/
	void Flush() noexcept;

private:

	/*Callback function when the mouse moves
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseMove(int x, int y) noexcept;

	/*Callback function when the Left mouse button is pressed
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseLeftButtonPressed(int x, int y) noexcept;

	/*Callback function when the Left mouse button is released
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseLeftButtonReleased(int x, int y) noexcept;

	/*Callback function when the Right mouse button is pressed
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseRightButtonPressed(int x, int y) noexcept;

	/*Callback function when the Right mouse button is released
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseRightButtonReleased(int x, int y) noexcept;

	/*Callback function when the mouse wheel goes up
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseWheelUp(int x, int y) noexcept;

	/*Callback function when the mouse wheel goes down
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseWheelDown(int x, int y) noexcept;

	/*Callback function when the middle mouse button is pressed
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseMiddleButtonPressed(int x, int y) noexcept;

	/*Callback function when the middle mouse button is released
	* @param x - Mouse x position
	* @param y - Mouse y position
	*/
	void OnMouseMiddleButtonReleased(int x, int y) noexcept;

	/*Callback function when the mouse leaves the app window*/
	void OnMouseLeave() noexcept;

	/*Callback function when the mouse enters the app window*/
	void OnMouseEnter() noexcept;

	/*Function that reduces the buffer when the max is exceeded */
	void TrimBuffer() noexcept;

	/*Function that checks the delta value to generate wheel move events
	* @param x - Mouse x position
	* @param y - Mouse y position
	* @param delta - Mouse heel delta value
	*/
	void OnWheelDelta(int x, int y, int delta) noexcept;

private:

	/*Member variables that stores the maximum size of the key bitset buffer*/
	static constexpr unsigned int m_nBufferSize = 16u;

	/*Member variables that saves the mouse x position*/
	int m_nX;

	/*Member variables that saves the mouse y position*/
	int m_nY;

	/*Member variables bool that says if left mouse button is pressed*/
	bool m_bLeftIsPressed = false;

	/*Member variables bool that says if right mouse button is pressed*/
	bool m_bRightIsPressed = false;

	/*Member variables bool that says if middle mouse button is pressed*/
	bool m_bMiddleIsPressed = false;

	/*Member variables bool that says if mouse is inside the app window*/
	bool m_bIsInWindow = false;

	/*Member variable that holds the current delta of the mouse wheel*/
	int m_nWheelDeltaCary = 0;

	/*Queue for the char events*/
	std::queue<CMouseEvent> m_MouseEventBuffer;
};
