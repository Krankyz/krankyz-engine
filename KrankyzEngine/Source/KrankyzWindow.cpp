/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#include "KrankyzWindow.h"
#include "../resource.h"
#include <sstream>	

//The Window Class
//static Window Class instance
CWindowMgr::CWindow CWindowMgr::CWindow::mwndClass;

//Constructor which gets handle to the instance
CWindowMgr::CWindow::CWindow() noexcept : mhInst(GetModuleHandle(nullptr))
{
	// Fill in the window class structure with parameters 
	// that describe the main window. 
	WNDCLASSEX _wc = { 0 };
	_wc.cbSize = sizeof(_wc);				// redraw if size changes 
	_wc.style = CS_OWNDC;					// Style of the window
	_wc.lpfnWndProc = HandleMsgSetup;		// points to window procedure
	_wc.cbClsExtra = 0;						// no extra class memory 
	_wc.cbWndExtra = 0;						// no extra window memory 
	_wc.hInstance = GetInstance();			// handle to instance 
	_wc.hIcon = static_cast<HICON>(LoadImage(mhInst, MAKEINTRESOURCE(IDI_APPICON), IMAGE_ICON, 32,32,0));							// Custom Icon
	_wc.hCursor = nullptr;					// Custom cursor
	_wc.hbrBackground = nullptr;			// Custom background
	_wc.lpszMenuName = nullptr;				// Menu Name
	_wc.lpszClassName = GetName();			// name of window class 
	_wc.hIconSm = static_cast<HICON>(LoadImage(mhInst, MAKEINTRESOURCE(IDI_APPICON), IMAGE_ICON, 16, 16, 0));						// small custom icon 	

	// Register the window class. 
	RegisterClassEx(&_wc);
}

CWindowMgr::CWindow::~CWindow()
{
	UnregisterClass(mwndClassName, GetInstance());
}

const char* CWindowMgr::CWindow::GetName() noexcept
{
	return mwndClassName;
}

HINSTANCE CWindowMgr::CWindow::GetInstance() noexcept
{
	return mwndClass.mhInst;
}


//The Window Manager Class

CWindowMgr::CWindowMgr(int width, int height, const char* name) : mwidth(width), mheight(height), mhWnd(0)
{
	//Calculate window size based on desired client region size
	RECT _wr;
	_wr.left = 100;
	_wr.right = width + _wr.left;
	_wr.top = 100;
	_wr.bottom = height + _wr.top;

	//Error catching
	if (AdjustWindowRect(&_wr, WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, FALSE) == 0)
	{
		throw KRANKYZ_LAST_EXCEPT();
	}

	// Create the main window. 
	HWND hwnd = CreateWindow(
		CWindow::GetName(),											// name of window class 
		name,														// title-bar string 
		WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_MAXIMIZEBOX,	// top-level window 
		CW_USEDEFAULT,												// default horizontal position 
		CW_USEDEFAULT,												// default vertical position 
		_wr.right - _wr.left,										// default width 
		_wr.bottom - _wr.top,										// default height 
		nullptr,													// no owner window 
		nullptr,													// use class menu 
		CWindow::GetInstance(),										// handle to application instance 
		this);														// window-creation data 

	//Update member variables
	mhWnd = hwnd;

	//Check if it successfully created the window
	if (hwnd == nullptr || mhWnd == nullptr)
	{
		throw KRANKYZ_LAST_EXCEPT();
	}

	// Show the window and send a WM_PAINT message to the window procedure. 
	ShowWindow(hwnd, SW_SHOWDEFAULT);
}

LRESULT WINAPI CWindowMgr::HandleMsgSetup(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
{
	//Use to create parameter passed in from CreateWindow() to store window calss pointer
	if (msg == WM_NCCREATE)
	{
		//Extract pointer to window class fro the creation data
		const CREATESTRUCTW* const _pCreate = reinterpret_cast<CREATESTRUCTW*>(lParam);
		CWindowMgr* const _pWnd = static_cast<CWindowMgr*>(_pCreate->lpCreateParams);

		//Set WinAPI-managed user data to store pointer to window class
		SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(_pWnd));

		//Set message procedure to normal (non-setup) handler now that setup is finished
		SetWindowLongPtr(hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&CWindowMgr::HandleMsgThunk));

		//Forward message to window class handler
		return _pWnd->HandleMsg(hWnd, msg, wParam, lParam);
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

LRESULT WINAPI CWindowMgr::HandleMsgThunk(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
{
	//Retrieve pointer to the window class
	CWindowMgr* const _pWnd = reinterpret_cast<CWindowMgr*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));

	//Forward message to window class handler
	return _pWnd->HandleMsg(hWnd, msg, wParam, lParam);
}

LRESULT CWindowMgr::HandleMsg(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
{
	switch (msg)
	{
		//since a destructor is in place for the window, We don't want DefProc to handle this message. Hence why we return 0
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;

		//Clear keystate when the window loses focus to prevent input getting stuck
		case WM_KILLFOCUS:
			m_CKeyboard.ClearState();
			break;

		/***********************KEYBOARD MESSAGES**************************/
		case WM_KEYDOWN:
		//Handle system keys so we can track ALT
		case WM_SYSKEYDOWN:
			if (!(lParam & 0x40000000) || m_CKeyboard.IsAutoRepeatEnabled()) //since bit 30 tracks if a key was down before the message was sent we check the state here
			{
				m_CKeyboard.OnKeyPressed(static_cast<unsigned char>(wParam));
			}
			break;
		case WM_KEYUP:
		case WM_SYSKEYUP:
			m_CKeyboard.OnKeyReleased(static_cast<unsigned char>(wParam));
			break;
		case WM_CHAR:
			m_CKeyboard.OnChar(static_cast<unsigned char>(wParam));
			break;
		/*********************END KEYBOARD MESSAGES************************/

		/*************************Mouse MESSAGES***************************/
		case WM_MOUSEMOVE:
		{
			const POINTS _pt = MAKEPOINTS(lParam);

			//In App region -> Log move and log enter plus capture mouse
			if (_pt.x >= 0 && _pt.x < mwidth && _pt.y > 0 && _pt.y < mheight)
			{
				m_CMouse.OnMouseMove(_pt.x, _pt.y);
				if (!m_CMouse.IsMouseInWindow())
				{
					SetCapture(hWnd);
					m_CMouse.OnMouseEnter();
				}
			}
			else //Not in App window -> log move / maintain capture if button down
			{
				if (wParam & (MK_LBUTTON | MK_RBUTTON))
				{
					m_CMouse.OnMouseMove(_pt.x, _pt.y);
				}
				else //Button up -> Release capture / log event for leaving
				{
					ReleaseCapture();
					m_CMouse.OnMouseLeave();
				}
			}

			break;
		}
		case WM_LBUTTONDOWN:
		{	
			const POINTS _pt = MAKEPOINTS(lParam);
			m_CMouse.OnMouseLeftButtonPressed(_pt.x, _pt.y);
			break;
		}
		case WM_LBUTTONUP:
		{	
			const POINTS _pt = MAKEPOINTS(lParam);
			m_CMouse.OnMouseLeftButtonReleased(_pt.x, _pt.y);
			break;
		}
		case WM_RBUTTONDOWN:
		{	
			const POINTS _pt = MAKEPOINTS(lParam);
			m_CMouse.OnMouseRightButtonPressed(_pt.x, _pt.y);
			break;
		}
		case WM_RBUTTONUP:
		{	
			const POINTS _pt = MAKEPOINTS(lParam);
			m_CMouse.OnMouseRightButtonReleased(_pt.x, _pt.y);
			break;
		}
		case WM_MOUSEWHEEL:
		{
			const POINTS _pt = MAKEPOINTS(lParam);
			const int _delta = GET_WHEEL_DELTA_WPARAM(wParam);
			m_CMouse.OnWheelDelta(_pt.x, _pt.y, _delta);
			break;
		}
		case WM_MBUTTONDOWN:
		{
			const POINTS _pt = MAKEPOINTS(lParam);
			m_CMouse.OnMouseMiddleButtonPressed(_pt.x, _pt.y);
			break;
		}
		case WM_MBUTTONUP:
		{
			const POINTS _pt = MAKEPOINTS(lParam);
			m_CMouse.OnMouseMiddleButtonReleased(_pt.x, _pt.y);
			break;
		}
		/***********************END MOUSE MESSAGES************************/
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void CWindowMgr::SetWindowTitle(const std::string& title)
{
	if (SetWindowText(mhWnd, title.c_str()) == 0)
	{
		throw KRANKYZ_LAST_EXCEPT();
	}
}

std::optional<int> CWindowMgr::ProcessMessages()
{
	//Message pump
	MSG _msg;

	//Loop to process messages
	while (PeekMessage(&_msg, nullptr, 0, 0, PM_REMOVE))
	{
		//The get message had an internal error
		if (_msg.message == WM_QUIT)
		{
			return _msg.wParam;
		}

		//Will not modify message but can generate auxiliary messages in special cases
		TranslateMessage(&_msg);

		//Passes the message to the window procedure
		DispatchMessage(&_msg);

	}
	return {};
}

CWindowMgr::~CWindowMgr()
{
	DestroyWindow(mhWnd);
}

//Window Exception Class
CWindowMgr::CWindowMgrException::CWindowMgrException(int line, const char* file, HRESULT hr) : CKrankyzException(line, file), mHr(hr)
{
}

const char* CWindowMgr::CWindowMgrException::what() const noexcept
{
	std::ostringstream oss;
	oss << GetType() << std::endl
		<< "[Error Code] " << GetErrorCode() << std::endl
		<< "[Description] " << GetErrorString() << std::endl
		<< GetOriginString();
	mstrWhatBuffer = oss.str();
	return mstrWhatBuffer.c_str();
}

const char* CWindowMgr::CWindowMgrException::GetType() const noexcept
{
	return "Krankyz Window Exception";
}

std::string CWindowMgr::CWindowMgrException::TranslateErrorCode(HRESULT hr)
{
	char* _pMsgBuff = nullptr;
	DWORD _dwMsgLen = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		nullptr, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		reinterpret_cast<LPSTR>(&_pMsgBuff), 0, nullptr
	);

	if (_dwMsgLen == 0)
	{
		return "Unidentified error code";
	}

	std::string strErrorString = _pMsgBuff;
	LocalFree(_pMsgBuff);
	return strErrorString;
}

HRESULT CWindowMgr::CWindowMgrException::GetErrorCode() const noexcept
{
	return mHr;
}

std::string CWindowMgr::CWindowMgrException::GetErrorString() const noexcept
{
	return TranslateErrorCode(mHr);
}
