/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#include "KrankyzException.h"
#include <sstream>

CKrankyzException::CKrankyzException(int line, const char* file) noexcept : mLine(line), mstrFile(file)
{
}

const char* CKrankyzException::what() const noexcept
{
	std::ostringstream oss;
	oss << GetType() << std::endl << GetOriginString();
	mstrWhatBuffer = oss.str();
	return mstrWhatBuffer.c_str();
}

const char* CKrankyzException::GetType() const noexcept
{
	return "Krankyz Exception";
}

int CKrankyzException::GetLine() const noexcept
{
	return mLine;
}

const std::string& CKrankyzException::GetFile() const noexcept
{
	return mstrFile;
}

std::string CKrankyzException::GetOriginString() const noexcept
{
	std::ostringstream oss;
	oss << "[File] " << mstrFile << std::endl
		<< "[Line] " << mLine;
	return oss.str();
}
