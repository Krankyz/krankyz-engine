/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#include "KrankyzWinDefs.h"
#include "Mouse.h"

std::pair<int, int> CMouse::GetPosition() const noexcept
{
	return { m_nX , m_nY };
}

int CMouse::GetPositionX() const noexcept
{
	return m_nX;
}

int CMouse::GetPositionY() const noexcept
{
	return m_nY;
}

bool CMouse::IsLeftButtonPressed() const noexcept
{
	return m_bLeftIsPressed;
}

bool CMouse::IsRightButtonPressed() const noexcept
{
	return m_bRightIsPressed;
}

bool CMouse::IsMiddleButtonPressed() const noexcept
{
	return m_bMiddleIsPressed;
}

bool CMouse::IsMouseInWindow() const noexcept
{
	return m_bIsInWindow;
}

CMouse::CMouseEvent CMouse::Read() noexcept
{
	if (m_MouseEventBuffer.size() > 0u)
	{
		CMouse::CMouseEvent _mouseEvent = m_MouseEventBuffer.front();
		m_MouseEventBuffer.pop();
		return _mouseEvent;
	}
	else
	{
		return CMouse::CMouseEvent();
	}
}

void CMouse::Flush() noexcept
{
	m_MouseEventBuffer = std::queue<CMouseEvent>();
}

void CMouse::OnMouseMove(int x, int y) noexcept
{
	m_nX = x;
	m_nY = y;
	
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::Move, *this));
	TrimBuffer();
}

void CMouse::OnMouseLeftButtonPressed(int x, int y) noexcept
{
	m_bLeftIsPressed = true;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::LPress, *this));
	TrimBuffer();
}

void CMouse::OnMouseLeftButtonReleased(int x, int y) noexcept
{
	m_bLeftIsPressed = false;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::LRelease, *this));
	TrimBuffer();
}

void CMouse::OnMouseRightButtonPressed(int x, int y) noexcept
{
	m_bRightIsPressed = true;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::RPress, *this));
	TrimBuffer();
}

void CMouse::OnMouseRightButtonReleased(int x, int y) noexcept
{
	m_bRightIsPressed = false;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::RRelease, *this));
	TrimBuffer();
}

void CMouse::OnMouseWheelUp(int x, int y) noexcept
{
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::WheelUp, *this));
	TrimBuffer();
}

void CMouse::OnMouseWheelDown(int x, int y) noexcept
{
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::WheelDown, *this));
	TrimBuffer();
}

void CMouse::OnMouseMiddleButtonPressed(int x, int y) noexcept
{
	m_bMiddleIsPressed = true;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::MiddlePressed, *this));
	TrimBuffer();
}

void CMouse::OnMouseMiddleButtonReleased(int x, int y) noexcept
{
	m_bMiddleIsPressed = false;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::MiddleReleased, *this));
	TrimBuffer();
}

void CMouse::OnMouseLeave() noexcept
{
	m_bIsInWindow = false;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::Leave, *this));
	TrimBuffer();
}

void CMouse::OnMouseEnter() noexcept
{
	m_bIsInWindow = true;
	m_MouseEventBuffer.push(CMouse::CMouseEvent(CMouse::CMouseEvent::Type::Enter, *this));
	TrimBuffer();
}

void CMouse::TrimBuffer() noexcept
{
	while (m_MouseEventBuffer.size() > m_nBufferSize)
	{
		m_MouseEventBuffer.pop();
	}
}

void CMouse::OnWheelDelta(int x, int y, int delta) noexcept
{
	m_nWheelDeltaCary += delta;

	//Generate events fro every 120
	while (m_nWheelDeltaCary >= WHEEL_DELTA)
	{
		m_nWheelDeltaCary -= WHEEL_DELTA;
		OnMouseWheelUp(x, y);
	}
	while (m_nWheelDeltaCary <= WHEEL_DELTA)
	{
		m_nWheelDeltaCary += WHEEL_DELTA;
		OnMouseWheelDown(x, y);
	}
}
