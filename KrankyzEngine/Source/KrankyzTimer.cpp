/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#include "KrankyzTimer.h"

cKrankyzTimer::cKrankyzTimer()
	: m_lastTime(std::chrono::steady_clock::now())
{
}

float cKrankyzTimer::Mark()
{
	const auto _oldTime = m_lastTime;
	m_lastTime = std::chrono::steady_clock::now();
	const std::chrono::duration<float> _frameTime = m_lastTime - _oldTime;
	return _frameTime.count();
}

float cKrankyzTimer::Peek() const
{
	return std::chrono::duration<float>(std::chrono::steady_clock::now() - m_lastTime).count();
}
