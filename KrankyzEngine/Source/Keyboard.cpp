/********************************************************************************************
*	Krankyz Engine																			*
*	Copyright 2020 Krankyz Studio															*
*																							*
*	This file is part of Krankyz Engine.													*
*																							*
*	Krankyz Studio is free software: you can redistribute it and/or modify					*
*	it under the terms of the GNU General Public License as published by					*
*	the Free Software Foundation, either version 3 of the License, or						*
*	(at your option) any later version.														*
*																							*
*	The Krankyz Studio is distributed in the hope that it will be useful,					*
*	but WITHOUT ANY WARRANTY; without even the implied warranty of							*
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the							*
*	GNU General Public License for more details.											*
*																							*
*	You should have received a copy of the GNU General Public License						*
*	along with The Krankyz Studio.  If not, see <http://www.gnu.org/licenses/>.				*
*********************************************************************************************/
#include "Keyboard.h"

bool CKeyboard::KeyIsPressed(unsigned char keyCode) const noexcept
{
	return m_KeyStates[keyCode];
}

CKeyboard::CKeyboardEvent CKeyboard::ReadKey() noexcept
{
	if (m_KeyBuffer.size() > 0u)
	{
		CKeyboard::CKeyboardEvent _event = m_KeyBuffer.front();
		m_KeyBuffer.pop();
		return _event;
	}
	else
	{
		return CKeyboard::CKeyboardEvent();
	}
}

bool CKeyboard::KeyIsEmpty() const noexcept
{
	return m_KeyBuffer.empty();
}

void CKeyboard::FlushKey() noexcept
{
	m_KeyBuffer = std::queue<CKeyboardEvent>();
}

char CKeyboard::ReadChar() noexcept
{
	if (m_CharBuffer.size() > 0u)
	{
		unsigned char _szCharCode = m_CharBuffer.front();
		m_CharBuffer.pop();
		return _szCharCode;
	}
	else
	{
		return 0;
	}
}

bool CKeyboard::CharIsEmpty() const noexcept
{
	return m_CharBuffer.empty();
}

void CKeyboard::FlushChar() noexcept
{
	m_CharBuffer = std::queue<char>();
}

void CKeyboard::Flush() noexcept
{
	FlushKey();
	FlushChar();
}

void CKeyboard::EnableAutoRepeat() noexcept
{
	m_bAutoRepeatEnabled = true;
}

void CKeyboard::DisableAutoRepeat() noexcept
{
	m_bAutoRepeatEnabled = false;
}

bool CKeyboard::IsAutoRepeatEnabled() const noexcept
{
	return m_bAutoRepeatEnabled;
}

void CKeyboard::OnKeyPressed(unsigned char keyCode) noexcept
{
	m_KeyStates[keyCode] = true;
	m_KeyBuffer.push(CKeyboard::CKeyboardEvent(CKeyboard::CKeyboardEvent::Type::Press, keyCode));
	TrimBuffer(m_KeyBuffer);
}

void CKeyboard::OnKeyReleased(unsigned char keyCode) noexcept
{
	m_KeyStates[keyCode] = false;
	m_KeyBuffer.push(CKeyboard::CKeyboardEvent(CKeyboard::CKeyboardEvent::Type::Release, keyCode));
	TrimBuffer(m_KeyBuffer);
}

void CKeyboard::OnChar(char character) noexcept
{
	m_CharBuffer.push(character);
	TrimBuffer(m_CharBuffer);
}

void CKeyboard::ClearState() noexcept
{
	m_KeyStates.reset();
}

template<typename T>
void CKeyboard::TrimBuffer(std::queue<T>& buffer) noexcept
{
	while (buffer.size() > m_nBufferSize)
	{
		buffer.pop();
	}
}